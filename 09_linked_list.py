class Node():
    def __init__(self, data, next_node=None):
        self.data = data
        self.next_node = next_node


class Linked_List():
    def __init__(self, first_node):
        self.first_node = first_node
    
    def read(self, index):
        current_node = self.first_node
        current_index = 0

        while current_index < index:
            current_node = current_node.next_node
            current_index = current_index + 1
            if current_node == None:
                return None
        return current_node.data

    def search(self, data):
        current_index = 0
        current_node = self.first_node

        while current_node:
            if current_node.data == data:
                return current_index
            
            current_node = current_node.next_node
            current_index = current_index + 1
        return None

    def insert(self, index, data):
        current_node = self.first_node
        current_index = 0

        if index == current_index:
            newnode = Node(data, current_node)
            self.first_node = newnode

        while current_index < index:
            current_node = current_node.next_node
            current_index = current_index + 1

            if current_node == None:
                return None
        newnode = Node(data, current_node.next_node)
        current_node.next_node = newnode


    def get_list(self):
        out = []
        current_node = self.first_node
        while current_node:
            out.append(current_node.data)
            current_node = current_node.next_node
        return out

node_1 = Node("once")
node_2 = Node("upon")
node_1.next_node = node_2

node_3 = Node("a")
node_2.next_node = node_3

node_4 = Node("time")
node_3.next_node = node_4

linked_list = Linked_List(node_1)
linked_list2 = Linked_List(node_1)



print(str(type(linked_list.first_node)) + ' ' + str(type(node_1.next_node)))


def get_node(linked_list, index):
    i = 1
    ll = Linked_List(linked_list.first_node)
    while i <= index:
        ll = Linked_List(ll.first_node.next_node)
        i = i + 1
    return ll.first_node.data


linked_list2.insert(2, "hello")

# print(get_node(linked_list, 3))
# print(linked_list2.read(3))


print(linked_list2.search("upon"))

print(linked_list2.get_list())