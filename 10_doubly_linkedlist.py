class Node():
    def __init__(self, data=None, pnode=None, nnode=None):
        self.data = data
        self.pnode = pnode
        self.nnode = nnode

class DoublyLinkedList():
    def __init__(self, snode, lnode):
        self.snode = snode
        self.lnode = lnode

    def insert_at_end(self, x):
        k = Node(x)
        if self.snode:
            k.pnode=self.lnode
            self.lnode.nnode = k
            self.lnode = k
        else:
            self.snode = k
            self.lnode = k

    def remove_from_front(self):
        removed_node = self.snode
        self.snode = self.snode.nnode
        return removed_node

    def get(self):
        i = 0
        x = self.snode
        
        while x:
            print(x.data)
            x = x.nnode
            i = i + 1

