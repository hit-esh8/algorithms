# Linear search
# a = [4, 2, 7, 1, 3]
# n = 1
# i <- 3 


def search(n, a):
    for i, v in enumerate(a):
        if v == n:
            return i


def search_2(n, a):
    i = 0
    while i < len(a):
        if a[i] == n:
            return i
        i = i + 1

# Note:
# In case the array to search is ordered,
# while searching we can add an extra condition,
# if value at array[index] > searh_number
# then we could stop there itself and return None.
