# Checks if an array contains any duplicate values
# a = [1, 2, 5, 3, 5, 12, 18, 21, 2]
# True

# Using nested for loops
def has_duplicate_value_1(array):
    for i in range(len(array)):
        for j in range(len(array)):
            if i != j and array[i] == array[j]:
                return True
    
    return False


# Linear approach
def has_duplicate_value_2(array):
    found = []
    for i in range(len(array)):
        if array[i] in found:
            return True
        else:
            found.append(array[i])
    return False
