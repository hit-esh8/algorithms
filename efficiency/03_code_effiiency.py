# a   =  [3, 1, 4, 2]
# b   =  [4, 5, 3, 6]
# out =  [3, 4]

# Inefficient code
def intersection(a1, a2):
    intersect = []
    for i in range(len(a1)):
        for j in range(len(a2)):
            if a2[j] == a1[i]:
                intersect += [a1[i]]
    return intersect


def intersection_2(a1, a2):
    out = []
    i = 0
    while i < len(a1):
        j = 0
        while j < len(a2):
            if i != j and a2[j] not in out and a1[i] == a2[j]:
                out.append(a2.pop(j))
            j = j + 1
        i = i + 1
    return out
