# Return every other number from array (Alternate)
# Input: [1, 2, 3, 4, 5]
# Output: [2, 4] or [1, 3, 5]

# Inefficient code
def every_other_1(array):
    x = []
    for i in range(len(array)):
        if i % 2 == 0:
            x.append(array[i])
    return x


def every_other_2(array):
    x = []
    for i in array[::2]:
        x.append(i)
    return x


def every_other_3(array):
    x = []
    i = 0
    while i < len(array):
        x.append(array[i])
        i = i + 2
    return x
