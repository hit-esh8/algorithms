# Quick Sort using recursion
def partition(lp, rp):
    pi = rp
    pivot = a[pi]
    rp = rp - 1

    while True:
        while a[lp] < pivot:
            lp = lp + 1
        
        while a[rp] > pivot:
            rp = rp - 1
        
        if lp >= rp:
            break
        else:
            a[lp], a[rp] = a[rp], a[lp]
    
    a[lp], a[pi] = a[pi], a[lp]
    return lp


# initial arguments:
# li = start index of array
# ri = last index of array
def quicksort(li, ri):
    if ri - li <= 0:
        return
    
    pi = partition(li, ri)
    quicksort(li, pi - 1)
    quicksort(pi + 1, ri)


# Test:
# a = [0, 5, 2, 1, 6, 3]
# print(f'Before sort:\n{a}\n')
# quicksort(0, len(a) - 1)
# print(f'After sort:\n{a}')



"""
Quicksort using Classes
"""
class Sorting():
    def __init__(self, a):
        self.a = a

    def partition(self, lp, rp):
        p = rp
        rp = p - 1

        while True:
            while self.a[lp] < self.a[p]:
                lp = lp + 1
            
            while self.a[rp] > self.a[p]:
                rp = rp - 1
            
            if lp >= rp:
                break
            else:
                self.a[lp], self.a[rp] = self.a[rp], self.a[lp]
            
        self.a[p], self.a[lp] = self.a[lp], self.a[p]
        return lp

    def quicksort(self, lp, rp):
        if rp - lp <= 0:
            return

        p = self.partition(lp, rp)
        self.quicksort(lp, p - 1)
        self.quicksort(p + 1, rp)


# Test
a = [4, 2, 7, 1, 3]
s = Sorting(a)
print(s.a)
s.quicksort(0, len(a)-1)
print(s.a)