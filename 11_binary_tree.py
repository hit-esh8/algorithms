class TreeNode():
    def __init__(self, value=None, lchild=None, rchild=None):
        self.value = value
        self.lchild = lchild
        self.rchild = rchild


def search(node, x, ls=False):
    if node and ls:
        print(f'{node.value} {node}')

    if node is None or node.value == x:
        return node

    elif x < node.value:
        return search(node.lchild, x, ls)

    else: # x > node.value
        return search(node.rchild, x, ls)


def insert(x, node):
    if x > node.value:
        if node.rchild is None:
            node.rchild = TreeNode(x)
        else:
            insert(x, node.rchild)
    elif x < node.value:
        if node.lchild is None:
            node.lchild = TreeNode(x)
        else:
            insert(x, node.lchild)


a = TreeNode(4)
c = TreeNode(11)
b = TreeNode(10, a, c)
d = TreeNode(30)
f = TreeNode(40)
e = TreeNode(33, d, f)
g = TreeNode(52)
i = TreeNode(61)
h = TreeNode(56, g, i)
j = TreeNode(82)
l = TreeNode(95)
k = TreeNode(89, j, l)
m = TreeNode(25, b, e)
n = TreeNode(75, h, k)
o = TreeNode(50, m, n)
print(search(o, 33))
print('\n\n')
insert(45, o)
search(o, 45, True)