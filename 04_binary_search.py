# Binary Search
# Divide and search in an ordered array.
# With each step half of the elements will be eliminated for search.
# a = [1, 2, 3, 4, 7]
# n = 1
# i <- 3

def binary_search(n, a):
    l = 0
    h = len(a) - 1

    while l <= h:
        m = int((l + h) / 2)
        if a[m] == n:
            return m
        elif a[m] > n:
            h = m - 1
        elif a[m] < n:
            l = m + 1
    return None
