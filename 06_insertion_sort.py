# Insertion sort
# in each iteration, takes initial in memory, makes gap, compares it to all at
# left, moves bigger to right..
# a = [4, 2, 7, 1, 3]
# out <- [1, 2, 3, 4, 7]

def insertion_sort(a):
    i = 1
    while i < len(a):
        space = i
        ori = a[i]
        j = i
        while j > 0 and a[j - 1] > ori:
            a[j] = a[j - 1]
            space = j - 1
            j = j - 1
        a[space] = ori
        i = i + 1
    return a


# Recursive
def insertion_sort_R(a, n):
    if n > 0:
        insertion_sort_R(a, n-1)
        x = a[n]
        j = n-1
        while j >= 0 and a[j] > x:
            a[j+1] = a[j]
            j = j-1
        a[j+1] = x
    return a

# 4 types of steps
#                       Worst Case      Steps
# - removal             once/pass       === N - 1
# - comparison          1+2+3+...+N-1   === N Sq / 2
# - shift               1+2+3+...+N-1   === N Sq / 2
# - insert              once/pass       === N - 1
#                                       -----------------
#                                       N Square + 2N + 2
#                                       -----------------
# Big O - ignores constants             N Square + N
#                                       -----------------
# Big O - highest order of N            N Square