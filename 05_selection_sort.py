# Selection sort
# Selects and maintains a lowest index while looping in cycles.
# Gets sorted at left. Max 1 swap in a passthrough.
# a = [4, 2, 7, 1, 3]
# out <- [1, 2, 3, 4, 7]

def selection_sort(a):
    i = 0
    while i < len(a):
        low = i
        j = i + 1
        while j < len(a):
            if a[j] < a[low]:
                low = j
            j = j + 1
        if i != low:
            a[low], a[i] = a[i], a[low]
        i = i + 1
    return a
