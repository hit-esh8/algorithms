# music album ranking using selection sort
from pprint import pprint
music = [ {'album': 'RADIOHEAD', 'rank': 155},
        {'album': 'KISHORE KUMAR', 'rank': 141},
        {'album': 'THE BLACK KEYS', 'rank': 35},
        {'album': 'NEUTRAL MILK HOTEL', 'rank': 94},
        {'album': 'BECK', 'rank': 88},
        {'album': 'THE STROKES', 'rank': 61},
        {'album': 'WILCO', 'rank': 111} ]

def ranking_sort(m):
    i = 0
    while i < len(m):
        highest = i
        j = i
        while j < len(m) - 1:
            if m[j+1]['rank'] < m[highest]['rank']:
                highest = j + 1
            j = j + 1
        
        if highest != i:
            m[i], m[highest] = m[highest], m[i]
        i = i + 1
    return m

def table_format(data):
    first = 4
    second = 25
    third = 8

    i = 0
    print(((first+second+third) * '-').rjust(first+second+third))
    print('No.'.center(first) + 'Album'.center(second) + 'Rank'.rjust(third))
    print(((first+second+third) * '-').rjust(first+second+third))

    while i < len(data):
            print(str(i).center(first) + data[i]['album'].ljust(second) + str(data[i]['rank']).rjust(third))
            i += 1

table_format(ranking_sort(music))

pprint(music)
