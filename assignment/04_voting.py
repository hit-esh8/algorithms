# Inefficient
class Voting_1():
    def __init__(self, votes=[], candidate_vote_list={}):
        self.votes = votes
        self.candidate_vote_list = candidate_vote_list
    
    def add_votes(self, candidate):
        self.votes.append(candidate)

    def count_votes(self):
        for i, v in enumerate(self.votes):
            if v not in self.candidate_vote_list.keys():
                self.candidate_vote_list[v] = 1
            else:
                self.candidate_vote_list[v] += 1

    def get_votes(self):
        print('Vote Results: \n')
        for k, v in self.candidate_vote_list.items():
            print(f' - {k}\t\t{v} votes')


# 2. Efficient and simple
class Voting_2():
    def __init__(self, votes={}):
        self.votes = votes

    def add_votes(self, candidate):
        if candidate in self.votes:
            self.votes[candidate] += 1
        else:
            self.votes[candidate] = 1

    def get_votes(self):
        print("Votes Results:\n")
        for k, v in self.votes.items():
            print(f" - {k}\t\t{v} votes")


v1 = Voting_1()

v1.add_votes('Thomas Jefferson')
v1.add_votes('Abraham Lincoln')
v1.add_votes('John Adams')
v1.add_votes('John Adams')
v1.add_votes('Thomas Jefferson')
v1.add_votes('Thomas Jefferson')

v1.count_votes()
v1.get_votes()

print("\n\n")

v2 = Voting_2()

v2.add_votes('Thomas Jefferson')
v2.add_votes('Abraham Lincoln')
v2.add_votes('John Adams')
v2.add_votes('John Adams')
v2.add_votes('Thomas Jefferson')
v2.add_votes('Thomas Jefferson')

v2.get_votes()
