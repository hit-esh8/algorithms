# O(n) for unsorted array
def small(l):
    small = 0
    i = 1
    while i < len(l):
        if l[i] < l[small]:
            small = i
        i = i + 1
    return f'Smallest is {l[small]} at {small}'

def nth_small(l, n):
    i = 0
    while i < len(l) - 1:
        j = 0
        while j < len(l) - 1 - i:
            if l[j + 1] < l[j]:
                l[j+1], l[j] = l[j], l[j+1]
            j = j + 1
        i = i + 1
    return l[n]

def partition(l, r):
    p = r
    r = p - 1
    piv = arr[p]

    while True:
        while arr[l] < piv:
            l = l + 1
        while arr[r] > piv:
            r = r - 1
        if l >= r:
            break
        else:
            arr[l], arr[r] = arr[r], arr[l]
    arr[l], arr[p] = arr[p], arr[l]
    print(l)
    return l


dc = {}
def qsort(l, r, n):
    if r - l <=0:
        return

    p = partition(l, r)
    dc[p] = arr[p]

    if p in dc:
        print(f'found at {p} - {arr[p]}')

    if p == n:
        return arr[p]
    if p > n:
    # elif n < p:
        qsort(l, p - 1, n)
    if p < n:
        qsort(p + 1, r, n)








arr = [10, 2, 5, 6, 11, 3, 15]
print(arr)
qsort(0, len(arr) - 1, 6)
print(arr)
#print(nth_small(arr, 1))

#print(small(arr))

