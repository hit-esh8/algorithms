import os, shutil, json, datetime

# For single tuple to dict
# Join keys (meta-attributes) & values (tuple) and return dict
def tuple_to_dict(keys, values):
        keys = list(keys)
        values = list(values)
        return dict(zip(keys, values))

# For multiple tuples to list of dicts
# Input: keys, values as tuples Return: list of dicts with keys and values
def listall(keys, tuple_list):
        result = []
        for i in tuple_list:
            result.append(tuple_to_dict(keys, i))
        return result


# --- Files ---
# create folder
def cfo(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)

# remove folder & its contents
def rmfo(folder):
    shutil.rmtree(folder)

# create file
def cfi(file, data):
    if not os.path.exists(file):
        with open(file, 'w') as fb:
            fb.write(data)

# write file
def wfi(file, data):
    with open(file, 'w') as fb:
        fb.write(data)

# write json to file
def jwrite(file, data):
    with open(file, 'w') as fb:
        fb.write(json.dumps(data))

# read file
def rfi(file):
    with open(file, 'r') as fb:
        return fb.read()

# read json file
def jread(file):
    with open(file, 'r') as fb:
        return json.load(fb)


# input: int 0 or 1, if 0 return False, else if 1 return False 
def boo(x):
    if x == 0:
        return False
    elif x == 1:
        return True

# This is the default string format of date in Database
def date_format(ds, f='s'):
    if f == 's':
        format_string = '%b %d %Y'
    elif f == 'l':
        format_string = '%H-%M, %b %d %Y'
    return ds.strftime(format_string)

# def to_time(hours=5.5):
#     format = "%a %b %d %I:%M %p"
#     # Use timedeltas to add and subtract times.
#     time = now + datetime.timedelta(hours)
#     print(time.strftime(format), "India (UTC + 5.5)")
#     timestamp = datetime.timestamp(time)
#     print("timestamp =", timestamp)
