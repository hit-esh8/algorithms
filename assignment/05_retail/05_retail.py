from datetime import datetime
from d_fetcher import D_fetcher
from pprint import pprint
import os

DATA_FOLDER = os.getcwd()
DATA_FILE = os.path.join(DATA_FOLDER, './data/data.db')
db = D_fetcher(DATA_FILE)

def create(data_folder):
    global DATA_FOLDER
    DATA_FOLDER = data_folder
    DATA_FILE = os.path.join(DATA_FOLDER, './data/data.db')
    os.makedirs(os.path.join(DATA_FOLDER, 'data'), exist_ok=True)

    global db
    db = D_fetcher(DATA_FILE)
    c1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'name': 'TEXT' 
    }
    db.create(table_name='category', variables=c1)

    p1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'name': 'TEXT',
        'address': 'TEXT',
        'phone_nos': 'TEXT',
        'gst_no': 'TEXT', 
    }
    db.create(table_name='party', variables=p1)

    sd1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'stock_id': 'INT',
        'colour': 'TEXT',
        'size': 'TEXT',
        'qty': 'INT',
        'price': 'FLOAT', 
    }
    db.create(table_name='stockdetail', variables=sd1)

    s1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'purchase_id': 'INT',
        'cat_id': 'INT',
        'name': 'TEXT',
        'model': 'INT',
        'total_units': 'INT',
        'discount_perc': 'FLOAT',
        'tax': 'FLOAT',
        'price': 'FLOAT'
    }
    db.create(table_name='stock', variables=s1)

    pu1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'date': 'TEXT',
        'par_id': 'TEXT',
        'invoice_no': 'TEXT',
        'total_amount': 'FLOAT',
        'tax_amount': 'FLOAT'
    }
    db.create(table_name='purchase', variables=pu1)

class Printer():
    def __init__(self, dc=None):
        self.dc = dc

    def pretty(self, dc):
        for i, value in dc.items():
            print(f'{i}:\t\t{value}')

p = Printer()

class Table():
    def __init__(self):
        self.version = 0.1

    # helper function (common in get() & listall())
    def tuple_to_dict(self, tp):
        keys = (list(vars(self).keys()))[2:]
        values = list(tp)
        return dict(zip(keys, values))

    # gets by uid and converts to dict
    def get(self, uid):
        values = db.get(self.table_name, uid, key='uid')[0]
        # this result gets tuple, lets convert to dict & return
        # keys = (list(vars(self).keys()))[2:]
        # values = list(values)
        return self.tuple_to_dict(values)
    
    # list all in a table. returns list of dicts.
    def listall(self):
        tuple_list =  db.ls(self.table_name)
        result = []
        for i in tuple_list:
            result.append(self.tuple_to_dict(i))
        return result


class Category(Table):
    def __init__(self, uid=None, name=None):
        super(Category, self).__init__()
        self.table_name ='category'
    
        self.uid = uid
        self.name = name

    def todict(self):
        return {'name': self.name}

    def add(self):
        # db.inserts("INSERT INTO category(name) VALUES ('" + self.name + "');")
        db.insert2(self.table_name, self.todict())


class Party(Table):
    def __init__(self, uid=None, name=None, address=None, phone_nos=None, gst_no=None):
        super(Party, self).__init__()
        self.table_name = 'party'
        self.uid = uid
        self.name = name
        self.address = address
        self.phone_nos = phone_nos
        self.gst_no = gst_no

    def todict(self):
        return {'name': self.name, 'address': self.address, 'phone_nos': self.phone_nos, 'gst_no': self.gst_no}

    def add(self):
        db.insert2(self.table_name, self.todict())

class StockDetail():
    def __init__(self, uid=0, stock_id=0, price=0, colour='black', size=6, qty=1):
        super(StockDetail, self).__init__()
        self.table_name = 'stockdetail'
        self.uid = uid
        self.stock_id = stock_id
        self.colour = colour
        self.size = size
        self.qty = qty
        self.price = price

    def todict(self):
        return {'stock_id': self.stock_id, 'colour': self.colour, 'size': self.size, 'qty': self.qty, 'price': self.price}

    def add(self):
        db.insert2(self.table_name, self.todict())


class Stock():
    def __init__(self, uid=None, purchase_id=None, cat_id=None, name=None, model=None, total_units=None, discount_perc=None, tax=None, price=None):
        super(Stock, self).__init__()
        self.uid = uid
        self.purchase_id = purchase_id
        self.cat_id = cat_id
        self.name = name
        self.model = model
        self.total_units = total_units
        self.discount_perc = discount_perc
        self.tax = tax
        self.price = price

        self.category = Category()
        self.stock_detail = [ StockDetail() ]

    def todict(self):
        return {'purchase_id': self.purchase_id, 'cat_id': self.cat_id, 'name': self.name, 'model': self.model, 'total_units': self.total_units, 'discount_perc': self.discount_perc, 'tax': self.tax, 'price': self.price}

    def add(self):
        db.insert2(self.table_name, self.todict())

    def get_stockdetails(self):
        return self.get('stockdetails', self.stock_id, 'stock_id')


class Purchase():
    def __init__(self, uid, date, par_id, invoice_no, total_amount, tax_amount):
        super(Purchase, self).__init__()
        self.uid = uid
        self.date = date
        self.par_id = par_id
        self.invoice_no = invoice_no
        self.total_amount = total_amount
        self.tax_amount = tax_amount
        self.party = Party()
        self.stock = [ Stock() ]

    def todict(self):
        return {'date': self.date, 'part_id': self.par_id, 'invoice_no': self.invoice_no, 'total_amount': self.total_amount, 'tax_amount': self.tax_amount}

    def add(self):
        insert2(self.table_name, self.todict())

    def show_details(self):
        print(f'Purchase id {self.uid}')
        print(f'Date {self.date}')
        print(f'Invoice No {self.invoice_no}')
        print(f'Party { self.party.name}')
        for stk in self.stock:
            print(f'\n - Stock Name\t\t{stk.name}')
            print(f' - Stock Category\t{ stk.category.name}')
            print(f' - Stock model\t\t{ stk.model}')
            print(f' - Stock Price\t\t{ stk.price}')
            for std in stk.stock_detail:
                print(f'  -- {std.uid} | {std.stock_id} | Color {std.colour} | Size {std.size} | Qty {std.qty} | Price {std.price}')


# class Receipt(Table):
#     def __init__(self, uid, date, ):
#         super(Receipt, self).__init__()
#         self.uid


# def main():
#     #create(os.getcwd())

# main()




# Testing area ===>
ccdss = Category()
for i in ccdss.listall():
    p.pretty(i)


cat = Category()

purc = Purchase(1, datetime.now(), 3, 'ghdhh627', 28001, 3290)
purc.party = Party(name='Pooja Footwear', address='Sultanpet\nBangalore', phone_nos='080-22777279', gst_no='uuue8838999288')
purc.party.add()
#al2 = purc.party.listall()
#for i in al2:
#    print(i)

a2 = Party()
gg = a2.get(1)
print('from get - ')
p.pretty(gg)

#purc.party.pretty_print()
cat1 = Category(2, 'Footwear')
stk1 = Stock(1, 1, 'Rexwood', '258', 4, 0.0, 5.0, 250)
stk1.category = cat1
#print(cat1.get(29))

stkdetail1 = StockDetail(1, 1, 499, 'Brown', 7, 1)
stkdetail2 = StockDetail(2, 1, 499, 'Brown', 8, 1)
stkdetail3 = StockDetail(3, 1, 499, 'Brown', 9, 1)
stkdetail4 = StockDetail(4, 1, 599, 'Brown', 10, 1)
stk1.stock_detail = [ stkdetail1, stkdetail2, stkdetail3, stkdetail4 ]


stk2 = Stock(2, 1, 'Lancer', 'Germany', 6, 0.0, 5.0, 735)
stk2.category = cat

stkdetail5 = StockDetail(5, 2, 999, 'Red', 6, 1)
stkdetail6 = StockDetail(6, 2, 999, 'Red', 7, 1)
stkdetail7 = StockDetail(7, 2, 999, 'Red', 8, 2)
stkdetail8 = StockDetail(8, 2, 999, 'Red', 9, 1)
stkdetail9 = StockDetail(9, 2, 999, 'Green', 10, 1)
stk2.stock_detail = [ stkdetail5, stkdetail6, stkdetail7, stkdetail8, stkdetail9 ]


purc.stock = [ stk1, stk2 ]

purc2 = Purchase(2, datetime.now(), 1, 'ghddefedhh627', 201, 90)
purc2.party = Party(2, 'PK Footwear', 'India', ['080-22777279', '9877629927'], 'uuue8838999288')
#purc2.party.pretty_print()

purclist = [ purc, purc2 ]
#for i in purclist:
#    print(i.party.name)

#print()
#print()
purc.show_details()

#print(f'\n\n{purc.party.address}')

# Col = Category()
# Col.name = 'favourite'
# Col.add()
# Col2 = Category()
# Col2.name = 'sindolphin'
# Col2.add()

#Cd = Category()
#print(Cd.get(3))

