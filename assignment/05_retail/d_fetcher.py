import sqlite3 as lite

class D_fetcher():
    def __init__(self, datafile=None):
        self.datafile = datafile
        self.con = lite.connect(self.datafile)
    
    def create(self, table_name, **variables):
        self.variables = variables
        self.table_name = table_name

        out = []
        for first in self.variables.values():
            for k, v in first.items():
                out.append(str(k) + ' ' + str(v))

        with self.con:
            cur = self.con.cursor()
            cur.execute('CREATE TABLE IF NOT EXISTS ' + self.table_name + '(' + ', '.join(out) + ')')


    def drop(self, table_name):
        self.table_name = table_name
        with self.con:
            cur = self.con.cursor()
            cur.execute("DROP TABLE IF EXISTS " + self.table_name)


    def insert(self, table_name, values, many=False):
        self.table_name = table_name
        self.values = values

        with self.con:
            cur = self.con.cursor()
            instruction = 'INSERT INTO ' + self.table_name + ' VALUES' + str(self.values)
            if many:
                l = len(self.values[0])
                s = l * '? '
                ll = []
                for x in range(l): ll+='?'

                cur.executemany('INSERT INTO ' + self.table_name + ' VALUES(' + ', '.join(ll) + ')', self.values)
            else:
                cur.execute('INSERT INTO ' + self.table_name + ' VALUES(' + str(self.values)+')')
            return cur.lastrowid


    def insert2(self, table_name, dict_values):
        values = ''
        arr = []
        for k, v in dict_values.items():
            arr.append(k)                
            if v.isdecimal():
                values += str(v) + ', '
            else:
                values += "'" + str(v) + "', "
        values = values.rstrip(', ') # remove extra from end

        instruction = 'INSERT INTO ' + table_name + '('
        for i in range(len(arr)):
            if i == len(arr) - 1:
                instruction += arr[i] + ') VALUES ('
            else:
                instruction += arr[i] + ', '
        instruction += values + ');'

        print(instruction)
        with self.con:
            cur = self.con.cursor()
            cur.execute(instruction)
            return cur.lastrowid


    # SQL statement insert
    def inserts(self, instruction):
        self.instruction = instruction

        print(self.instruction)
        with self.con:
            cur = self.con.cursor()
            cur.execute(self.instruction)
            return cur.lastrowid


    # list all in a table
    def ls(self, table_name):
        self.table_name = table_name
        with self.con:
            cur = self.con.cursor()
            cur.execute('SELECT * FROM ' + self.table_name)
            return cur.fetchall()


    # get by uid (*all tables have primary key uid)
    def get(self, table_name, uid, key):
        self.table_name = table_name
        with self.con:
            cur = self.con.cursor()
            cur.execute('SELECT * FROM ' + self.table_name + ' WHERE ' + key + '=' + str(uid))
            return cur.fetchall()
            #return cur.fetchone()