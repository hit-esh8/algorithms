## Tables
| user |   |   |   |   |   |   |
| - | - | - | - | - | - | - |
| uid | date | name | pwd | email | phone | role | active

| category |   |
| - | - |
| uid | name |



Here `operator` is  `[equal | less | great]` 


| tax_structure |   |   |   |   |   |
| - | - | - | - | - | - |
| uid | category_id | operator | amt | tax | desc

| party |   |   |   |   |   |
| - | - | - | - | - | - |
| uid | name | address | email | phone | gst

| purchase |   |   |   |   |   |
| - | - | - | - | - | - |
| uid | date | party_id | invoice_no | tax_amt | total_amt

| stock |   |   |   |   |   |   |   |   |
| - | - | - | - | - | - | - | - | - |
| uid | purchase_id | category_id | name | model | total_units | flat_discount | tax | cost_price

| stock_detail |   |   |   |   |   |
| - | - | - | - | - | - |
| uid | stock_id | color | size | qty | sell_price

| receipt |   |   |   |   |   |
| - | - | - | - | - | - |
| uid | date | desc | global_discount | paymode_id | total_amt |

| receipt_item |   |   |   |   |   |   |   |
| - | - | - | - | - | - | - | - | 
| uid | receipt_id | stock_detail_id | price | flat_discount | qty | amt | paymode_id |

| paymode |   |   |   |
| - | - | - | - |
| uid | cash | card | online
