from data import Data as Db

def create(data_file):
    db = Db(data_file)
    u1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'date': 'TEXT',
        'name': 'TEXT',
        'pwd': 'TEXT',
        'email': 'TEXT',
        'phone': 'TEXT',
        'role': 'INTEGER',
        'active': 'INTEGER'
    }
    db.create(table_name='user', variables=u1)

    c1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'name': 'TEXT' 
    }
    db.create(table_name='category', variables=c1)

    ts1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'category_id': 'INTEGER',
        'operator': 'INTEGER',
        'amt': 'INTEGER',
        'tax': 'INTEGER',
        'desc': 'TEXT'
    }
    db.create(table_name='tax_structure', variables=ts1)

    pa1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'name': 'TEXT',
        'address': 'TEXT',
        'email': 'TEXT',
        'phone': 'TEXT',
        'gst': 'TEXT'
    }
    db.create(table_name='party', variables=pa1)

    pu1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'date': 'TEXT',
        'party_id': 'INTEGER',
        'invoice_no': 'TEXT',
        'tax_amt': 'INTEGER',
        'total_amt': 'INTEGER'
    }
    db.create(table_name='purchase', variables=pu1)

    st1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'purchase_id': 'INTEGER',
        'category_id': 'INTEGER',
        'name': 'TEXT',
        'model': 'TEXT',
        'total_units': 'INTEGER',
        'flat_discount': 'INTEGER',
        'tax': 'INTEGER',
        'cost_price': 'INTEGER'
    }
    db.create(table_name='stock', variables=st1)

    std1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'stock_id': 'INTEGER',
        'color': 'TEXT',
        'size': 'INTEGER',
        'qty': 'INTEGER',
        'sell_price': 'INTEGER'
    }
    db.create(table_name='stock_detail', variables=std1)

    r1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'date': 'TEXT',
        'desc': 'TEXT',
        'global_discount': 'INTEGER',
        'paymode_id': 'INTEGER',
        'total_amt': 'INTEGER'
    }
    db.create(table_name='receipt', variables=r1)

    ri1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'receipt_id': 'INTEGER',
        'stock_detail_id': 'INTEGER',
        'price': 'INTEGER',
        'flat_discount': 'INTEGER',
        'qty': 'INTEGER',
        'amt': 'INTEGER',
        'paymode_id': 'INTEGER'
    }
    db.create(table_name='receipt_item', variables=ri1)

    pay1 = {
        'uid': 'INTEGER PRIMARY KEY',
        'cash': 'INTEGER',
        'card': 'INTEGER',
        'online': 'INTEGER'
    }
    db.create(table_name='paymode', variables=pay1)
