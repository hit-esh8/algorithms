import os
import sqlite3 as lite
# import psycopg2

class Data():
    def __init__(self, datafile=None):
        # DATABASE_URL = os.environ['DATABASE_URL']
        # self.con = psycopg2.connect(DATABASE_URL, sslmode='require')
        self.con = lite.connect(datafile)

    def create(self, table_name, **variables):
        out = []
        for first in variables.values():
            for k, v in first.items():
                out.append(str(k) + ' ' + str(v))

        with self.con:
            cur = self.con.cursor()
            inst = 'CREATE TABLE IF NOT EXISTS ' + table_name + '(' + ', '.join(out) + ')'
            cur.execute(inst)
            self.con.commit()


    def drop(self, table_name):
        with self.con:
            cur = self.con.cursor()
            cur.execute("DROP TABLE IF EXISTS " + table_name)


    def insert(self, table_name, values, many=False):
        with self.con:
            cur = self.con.cursor()
            instruction = 'INSERT INTO ' + table_name + ' VALUES' + str(values)
            if many:
                l = len(values[0])
                s = l * '? '
                ll = []
                for x in range(l): ll+='?'

                cur.executemany('INSERT INTO ' + table_name + ' VALUES(' + ', '.join(ll) + ')', values)
            else:
                cur.execute('INSERT INTO ' + table_name + ' VALUES' + str(values))
            return cur.lastrowid


    def insert2(self, table_name, dict_values):
        values = ''
        arr = []
        for k, v in dict_values.items():
            arr.append(k)
            if str(v).isdecimal():
                values += str(v) + ', '
            else:
                values += "'" + str(v) + "', "
        values = values.rstrip(', ') # remove extra from end

        instruction = 'INSERT INTO ' + table_name + '('
        for i in range(len(arr)):
            if i == len(arr) - 1:
                instruction += arr[i] + ') VALUES ('
            else:
                instruction += arr[i] + ', '
        instruction += values + ');'

        with self.con:
            cur = self.con.cursor()
            cur.execute(instruction)
            return cur.lastrowid


    # SQL statement insert
    def inserts(self, instruction):
        with self.con:
            cur = self.con.cursor()
            cur.execute(instruction)
            return cur.lastrowid


    def update(self, table_name, variables, key, keyvalue):
        values = ''
        for k, v in variables.items():
            values += str(k) + '='
            if str(v).isdecimal():
                values += str(v) + ', '
            else:
                values += "'" + str(v) + "', "
        values = values.rstrip(', ') # remove extra from end
        instruction = 'UPDATE ' + table_name + ' SET ' + values + ' WHERE ' + key + '=' + str(keyvalue)

        with self.con:
            cur = self.con.cursor()
            cur.execute(instruction)
            return cur.lastrowid


    def delete(self, table_name, key, keyvalue):
        instruction = 'DELETE FROM ' + table_name + ' WHERE ' + key + '=' + str(keyvalue)
        with self.con:
            cur = self.con.cursor()
            cur.execute(instruction)


    # get by uid (*all tables have primary key uid)
    def manual_execute(self, instruction):
        with self.con:
            cur = self.con.cursor()
            cur.execute(instruction)
            return cur.fetchall()


    # list all in a table with pagings
    def ls(self, table_name, sort_key=None, sort_by=None, page_size=None, page_no=None):
        with self.con:
            cur = self.con.cursor()
            inst = 'SELECT * FROM ' + table_name
            if page_no and page_size:
                page_no = page_no - 1
                upp = (page_no * page_size) + 1
                inst += ' WHERE uid >= ' + str(upp) + ' LIMIT ' + str(page_size)
            if sort_by and sort_key:
                inst += ' ORDER BY ' + sort_key + ' ' + sort_by
            cur.execute(inst + ';')
            return cur.fetchall()


    # get all by a key's value (*all tables have primary key uid)
    def get(self, table_name, key, value):
        with self.con:
            cur = self.con.cursor()
            if not str(value).isdecimal():
            #     value = str(value)
            # else:
                value = "'" + str(value) + "'"
            cur.execute('SELECT * FROM ' + table_name + ' WHERE ' + key + '=' + str(value))
            return cur.fetchall()
            #return cur.fetchone()

    # Check if a value exists in a particular column in a table.
    # returns as list of tuples results
    # 0th listitem then 0th tuple value
    # changed to True or False
    def exists(self, table_name, key, value):
        with self.con:
            cur = self.con.cursor()
            inst = "SELECT COUNT(*) FROM " + table_name + " WHERE " + key + "='" + str(value) + "';"
            cur.execute(inst)
            if cur.fetchall()[0][0] > 0:
                return True
        return False