from werkzeug.security import check_password_hash
from utils import *
from data import Data as Db

DATA_FOLDER = os.getcwd()
DATA_FILE = os.path.join(DATA_FOLDER, './data2/data.db')

# Classes
class Table():
    def __init__(self, name=None):
        self.name = name
        self.db = Db(DATA_FILE)

    def get_keys(self, ty='ls'):
        if self.name == 'user':
            return User().get_keys(ty)
        if self.name == 'category':
            return Category().get_keys(ty)
        if self.name == 'tax_structure':
            return TaxStructure().get_keys(ty)
        if self.name == 'party':
            return Party().get_keys(ty)
        if self.name == 'purchase':
            return Purchase().get_keys(ty)
        if self.name == 'stock':
            return Stock().get_keys(ty)
        if self.name == 'stock_detail':
            return StockDetail().get_keys(ty)
        if self.name == 'receipt':
            return Receipt().get_keys(ty)
        if self.name == 'receipt_item':
            return ReceiptItem().get_keys(ty)
        if self.name == 'paymode':
            return Paymode().get_keys(ty)


    def add(self, values=None):
        inserted = self.db.insert2(self.name, values)
        return inserted

    def get_list(self):
        values = self.db.ls(self.name)
        value_list = listall(self.get_keys(), values)
        return value_list

    def update(self, values=None, key='uid', keyvalue=None):
        self.db.update(self.name, variables=values, key=key, keyvalue=keyvalue)

    def remove(self, key='uid', keyvalue=None):
        self.db.delete(table_name=self.name, key=key, keyvalue=keyvalue)

    def get(self, key='uid', keyvalue=None):
        values = self.db.get(table_name=self.name, key=key, value=keyvalue)
        if values:
            return tuple_to_dict(keys=self.get_keys(), values=values[0])


class User(Table):
    def __init__(self, uid=None, date=None, name=None, pwd=None, email=None, phone=None, role=None, active=None):
        super(User, self).__init__(name='user')
        self.uid = uid
        self.date = date
        self.name = name
        self.pwd = pwd
        self.email = email
        self.phone = phone
        self.role = role
        self.active = active

    def get_keys(self, ty='ls'):
        res = { 'uid': self.uid, 'date': self.date, 'name': self.name, 'pwd': self.pwd, 'email': self.email, 'phone': self.phone, 'role': self.role, 'active': self.active}
        if ty == 'add':
            res.pop('uid')
        return res


class Category(Table):
    def __init__(self, uid=None, name=None):
        super(Category, self).__init__(name='category')
        self.uid = uid
        self.name = name

    def get_keys(self, ty='ls'):
        res = { 'uid': self.uid, 'name': self.name }
        if ty == 'add':
            res.pop('uid')
        return res


class TaxStructure(Table):
    def __init__(self, uid=None, category_id=None, operator=None, amt=None, tax=None, desc=None):
        super(TaxStructure, self).__init__(name='tax_structure')
        self.uid = uid
        self.category_id = category_id
        self.operator = operator
        self.amt = amt
        self.tax = tax
        self.desc = desc

    def get_keys(self, ty='ls'):
        res = { 'uid': self.uid, 'category_id': self.category_id, 'operator': self.operator, 'amt': self.amt, 'tax': self.tax, 'desc': self.desc }
        if ty == 'add':
            res.pop('uid')
        return res


class Party(Table):
    def __init__(self, uid=None, name=None, address=None, email=None, phone=None, gst=None):
        super(Party, self).__init__(name='party')
        self.uid = uid
        self.name = name
        self.address = address
        self.email = email
        self.phone = phone
        self.gst = gst

    def get_keys(self, ty='ls'):
        res = { 'uid': self.uid, 'name': self.name, 'address': self.address, 'email': self.email, 'phone': self.phone, 'gst': self.gst }
        if ty == 'add':
            res.pop('uid')
        return res


class Purchase(Table):
    def __init__(self, uid=None, date=None, party_id=None, invoice_no=None, tax_amt=None, total_amt=None):
        super(Purchase, self).__init__(name='purchase')
        self.uid = uid
        self.date = date
        self.party_id = party_id
        self.invoice_no = invoice_no
        self.tax_amt = tax_amt
        self.total_amt = total_amt

    def get_keys(self, ty='ls'):
        res = { 'uid': self.uid, 'date': self.date, 'party_id': self.party_id, 'invoice_no': self.invoice_no, 'tax_amt': self.tax_amt, 'total_amt': self.total_amt }
        if ty == 'add':
            res.pop('uid')
        return res

class Stock(Table):
    def __init__(self, uid=None, purchase_id=None, category_id=None, name=None, model=None, total_units=None, flat_discount=None, tax=None, cost_price=None):
        super(Stock, self).__init__(name='stock')
        self.uid = uid
        self.purchase_id = purchase_id
        self.category_id = category_id
        self.name = name
        self.model = model
        self.total_units = total_units
        self.flat_discount = flat_discount
        self.tax = tax
        self.cost_price = cost_price

    def get_keys(self, ty='ls'):
        res = { 'uid': self.uid, 'purchase_id': self.purchase_id, 'category_id': self.category_id, 'name': self.name, 'model': self.model, 'total_units': self.total_units, 'flat_discount': self.flat_discount, 'tax': self.tax, 'cost_price': self.cost_price}
        if ty == 'add':
            res.pop('uid')
        return res

class StockDetail(Table):
    def __init__(self, uid=None, stock_id=None, color=None, size=None, qty=None, sell_price=None):
        super(StockDetail, self).__init__(name='stock_detail')
        self.uid = uid
        self.stock_id = stock_id
        self.color = color
        self.size = size
        self.qty = qty
        self.sell_price = sell_price

    def get_keys(self, ty='ls'):
        res = { 'uid': self.uid, 'stock_id': self.stock_id, 'color': self.color, 'size': self.size, 'qty': self.qty, 'sell_price': self.sell_price}
        if ty == 'add':
            res.pop('uid')
        return res


class Receipt(Table):
    def __init__(self, uid=None, date=None, desc=None, global_discount=None, paymode_id=None, total_amt=None):
        super(Receipt, self).__init__(name='receipt')
        self.uid = uid
        self.date = date
        self.desc = desc
        self.global_discount = global_discount
        self.paymode_id = paymode_id
        self.total_amt = total_amt

    def get_keys(self, ty='ls'):
        res = { 'uid': self.uid, 'date': self.date, 'desc': self.desc, 'global_discount': self.global_discount, 'paymode_id': self.paymode_id, 'total_amt': self.total_amt }
        if ty == 'add':
            res.pop('uid')
        return res


class ReceiptItem(Table):
    def __init__(self, uid=None, receipt_id=None, stock_detail_id=None, price=None, flat_discount=None, qty=None, amt=None, paymode_id=None):
        super(ReceiptItem, self).__init__(name='receipt_item')
        self.uid = uid
        self.receipt_id = receipt_id
        self.stock_detail_id = stock_detail_id
        self.price = price
        self.flat_discount = flat_discount
        self.qty = qty
        self.amt = amt
        self.paymode_id = paymode_id

    def get_keys(self, ty='ls'):
        res = { 'uid': self.uid, 'receipt_id': self.receipt_id, 'stock_detail_id': self.stock_detail_id, 'price': self.price, 'flat_discount': self.flat_discount, 'qty': self.qty, 'amt': self.amt, 'paymode_id': self.paymode_id }
        if ty == 'add':
            res.pop('uid')
        return res


class Paymode(Table):
    def __init__(self, uid=None, cash=None, card=None, online=None):
        super(Paymode, self).__init__(name='paymode')
        self.uid = uid
        self.cash = cash
        self.card = card
        self.online = online

    def get_keys(self, ty='ls'):
        res = { 'uid': self.uid, 'cash': self.cash, 'card': self.card, 'online': self.online }
        if ty == 'add':
            res.pop('uid')
        return res