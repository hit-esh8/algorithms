import random
from pprint import pprint
from setup import create
from classes import *
from utils import *
from data import Data as Db
import os
from datetime import datetime
from flask_misaka import markdown, Misaka
from flask import Flask, jsonify, flash, render_template, request, redirect, url_for, session, send_file, send_from_directory, safe_join, abort, render_template_string
from flask_session import Session


app = Flask(__name__)
app.secret_key = b'_5#y2L"F4R8z\n\xec]/'
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"

Misaka(app, tables=True, skip_html=True, fenced_code=True, strikethrough=True, highlight=True)

# site
app.config["SITE_NAME"] = "pole-star"
app.config["SITE_TITLE"] = "Flying in Space"

Session(app)


DATA_FOLDER = os.getcwd()
DATA_FILE = os.path.join(DATA_FOLDER, './data2/data.db')
create(DATA_FILE)


class App():
    def __init__(self):
        self.ver = 0.1
    
    @app.route('/about')
    def about():
        cat = Table('category')
        value = { 'name': 'fello'}
        pprint(cat.get_list())
        res = ''
        for x in cat.get_list():
            for i, v in x.items():
                res += '\n' + i + ' ' + str(v)
        return res

    @app.route("/addcategory", methods=["POST"])
    def addcategory():
        cat = request.form.get("c_name")
        new_cat = Table('category')
        value = {'name': cat}
        va = new_cat.add(value)
        category = Table('category').get(keyvalue=int(va))
        return category


    @app.route("/addparty", methods=["POST"])
    def addparty():
        p_name = request.form.get("p_name")
        p_addr = request.form.get("p_addr")
        p_email = request.form.get("p_email")
        p_phone = request.form.get("p_phone")
        p_gst = request.form.get("p_gst")
        
        new_party = Table('party')
        value = {'name': p_name, 'address': p_addr, 'email': p_email, 'phone': p_phone, 'gst': p_gst}
        va = new_party.add(value)
        party = Table('party').get(keyvalue=int(va))
        return party

    
    @app.context_processor
    def inject_enumerate():
        return dict(enumerate=enumerate)

    @app.route('/party')
    def party():
        parties = Table('party').get_list()
        return render_template('party.html', parties=parties)


    @app.route('/index')
    def index():
        categories = Table('category').get_list()
        parties = Table('party').get_list()
        return render_template('post.html', categories=categories, parties=parties)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)

# stk = Table('stock')
# n = random.randint(2, 9)
# x = ''
# while n != 0:
#     x += random.choice(['a', 'b', 'c', 'd', 'e', 'f'])
#     n = n - 1
# value = { 'sname': x}
# stk.add(values=value)


# stkitem = Table('stock_item')
# value = { 'model': 'fello', 'price': 387}
# stkitem.add(values=value)

# pprint(stkitem.get_list())
# print('hello')
# pprint(stkitem.get(key='model', keyvalue='hello'))


# value = { 'model': 'belly', 'price': 891}
# stkitem.update(key='uid', keyvalue=5, values=value)
# pprint(stkitem.get(keyvalue=5))