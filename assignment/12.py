def histogram1(s):
    dc = {}
    for i in s.strip().lower():
        if i in dc:
            dc[i] += 1
        else:
            dc[i] = 1
    
    for k, v in dc.items():
        print(f"{k}: {'#' * v}")
    print(dc)

def histogram(s):
    s = s.lower()
    char_array = {}
    for i in range(len(s)):
        if s[i] != ' ' and s[i] != '\t' and s[i] != '\n':
            if s[i] in char_array:
                char_array[s[i]] += 1
            else:
                char_array[s[i]] = 1
    print(char_array)



histogram("Johny Johny Yes Papa Eating Sugar no Papa")