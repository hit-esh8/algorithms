# Traversing through a filesystem.
# Find subdirectories & print them.

import os

# 1. Linear way
def find_subdirs(directory):
    dir_list = os.listdir(directory)
    for i in dir_list:
        dpath = os.path.join(directory, i)
        if os.path.isdir(dpath):
            print(dpath)

# 2. Recursive
def find_subdirs_r(directory):
    print(directory)
    dir_list = os.listdir(directory)

    for i in dir_list:
        dpath = os.path.join(directory, i)
        if os.path.isdir(dpath):
            subdirs(dpath)
