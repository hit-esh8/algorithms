''' 2. You are given all numbers between 1, 2,..., n except one. Your task is to find 
    the missing number.
    For example, the first input line contains an integer n
                 the second line contains n - 1 numbers. Each number is distinct and 
                 between 1 and n (inclusive).
    Print the missing number. Example
    Input:
    5
    2 3 1 5
    Output:
    4
'''

def missing(n, lis):
    for i in range(1, n+1):
        if i not in lis:
            return i

print(missing(5, [2, 3, 1, 5]))
