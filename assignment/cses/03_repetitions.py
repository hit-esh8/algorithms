''' 3. You are given a DNA sequence: a string consisting of characters A, C, G, and T. 
    Your task is to find the longest repetion in the sequence. This is a maximum-
    length substring containing only one type of character. 13:38 14:50
    Input: The only input line contains a string of n characters.
    Output: Print one integer: the length of the longest repetion.
    Example:
    Input:
    ATTCGGGA
    Output:
    3
'''

def missing(s):
    rep = [[s[0], 1]]
    i = 0
    count = 1
    while i < len(s):
        if s[i] == s[i + 1]:
            rep[i-count][1] +=1
            count += 1
        else:
            count = 1
            rep.append([s[i+1], 1])
        i = i + 1
        print(rep)

    maxi = rep[0][1]
    print(maxi)
    for i in rep:
        if i[1] > maxi:
            maxi = i[1]
    return maxi

print(missing('ATTCGGGACCCCTTA'))