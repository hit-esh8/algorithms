""" We prepare an empty stack, and then we
    1. When there is an opening brace, that
       doesn't have a corressponding brace.
       Syntax Error #1
    2. There is a closing brace that was never preceded
       by a opening brace.
       Syntax Error #2
    3. When a closing brace is not the same type of brace as the
       immediately preceding opening brace.
       Syntax Error #3
"""

class linter():
    def __init__(self, code=''):
        self.brace_dict = { '{': '}', '[': ']', '(': ')'}
        self.code = code

    def check(self):
        oarray = []
        running_closed = []

        for i in self.code:
            if i in self.brace_dict.keys():
                oarray.append(i)
            if i in self.brace_dict.values():
                if oarray:
                    if self.brace_dict[oarray[-1]] == i:
                        print("founf " + i)
                    else:
                        print("opening not found " + str(i))
                    oarray.pop()
                else:
                    print("not found " + str(i))
        print(oarray)


def linter1(code):
    stack = []
    c = { '{': '}', '(': ')', '[': ']' }
    i = 0
    while i < len(code):
        if code[i] in c:
            stack.append(code[i])
        if code[i] in c.values():
            l = len(stack)
            if l != 0:
                print('Syntax Error Type #2')
            if c[stack[l-1]] == code[i]:
                print('both done')
            else:
                print('found only closing')
            
        i = i + 1
    print(stack)


asd = linter('void main (helloS})')
asd.check()