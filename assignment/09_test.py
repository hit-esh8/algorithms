x = [8, 2, 5, 10, 12, 20, 13, 1, 15]
xs = [1, 2, 5, 8, 10, 12, 13, 15, 20]

n = 10
# find 10

# unsorted array search
def linear_search(n, a):
    i = 0
    while i < len(a):
        if a[i] == n:
            return i
        i = i + 1
    return None
print(linear_search(n, x))


# sorted array search
def linear_search_s(n, a):
    i = 0
    while i < len(a):
        if a[i] == n:
            return i
        elif a[i] > n:
            break
        i = i + 1
    return None
print(linear_search_s(n, xs))


# sorted array search
def binary_search(n, a):
    s = 0
    e = len(a) - 1

    while s <= e:
        m = int((s + e) / 2)

        if a[m] == n:
            return m
        elif n < a[m]:
            e = m - 1
        elif n > a[m]:
            s = m + 1
    return None
print(binary_search(n, xs))


x = [8, 2, 5, 10, 12, 20, 13, 1, 15]
# bubble sort
def bubble_sort(a):
    print(a)
    i = 0
    while i < len(a) - 1:
        j = 0
        while j < len(a) - 1 - i:
            if a[j+1] < a[j]:
                tmp = a[j+1]
                a[j+1] = a[j]
                a[j] = tmp
            j = j + 1
        i = i + 1
    return a
print(bubble_sort(x))


x = [8, 2, 5, 10, 12, 20, 13, 1, 15]
# selecion sort
def selecion_sort(a):
    print(a)
    i = 0
    while i < len(a):
        small = i
        j = i + 1
        while j < len(a):
            if a[j] < a[small]:
                small = j
            j = j + 1
        if i != small:
            a[i], a[small] = a[small], a[i]
        i = i + 1
    return a
print(selecion_sort(x))


x = [8, 2, 5, 10, 12, 20, 13, 1, 15]
# insertion sort
def insertion_sort(a):
    print(a)
    i = 1
    while i < len(a):
        original = a[i]
        space = i
        j = i
        while j > 0 and a[j - 1] > original:
            a[j] = a[j - 1]
            space = j - 1
            j = j - 1
        a[space] = original
        i = i + 1
    return a
print(insertion_sort(x))


print('quicksort:')
a = [8, 2, 5, 10, 12, 20, 13, 1, 15]
# quicksort
def partition(lp, rp):
    p = rp
    rp = p - 1     
    while True:
        while a[lp] < a[p]:
            lp = lp + 1
        
        while a[rp] > a[p]:
            rp = rp - 1
        
        if lp >= rp:
            break
        else:
            a[lp], a[rp] = a[rp], a[lp]
    a[lp], a[p] = a[p], a[lp]
    return lp      

def quicksort(lp, rp):
    if rp - lp <= 0:
        return
        
    p = partition(lp, rp)
    quicksort(lp, p - 1)
    quicksort(p + 1, rp)

print(a)
quicksort(0, len(x) - 1)
print(a)
            

