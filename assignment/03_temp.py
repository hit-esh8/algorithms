# celc = (5 / 9) * (fahr - 32)
def fahr_cels(f):
    return (5 / 9) * (f - 32)

def print_temp_table():
    LOWER = 0
    UPPER = 300
    STEP = 20
    F = LOWER
    print('Temperature Table:\n(fahr)\t(cels)')
    while F <= UPPER:
        print('{:5.1f} {:7.2f}'.format(F, fahr_cels(F)))
        F = F + STEP

print_temp_table()
