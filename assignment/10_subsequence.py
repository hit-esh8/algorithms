''' Given a string S and a set of words D, find the longest word in D
    that is a subsequence of S.
    Eg. input of S = "abppplee" and D = {"able", "ale", 
    "apple", "bale", "kangaroo"} the correct output would be "apple"
'''

# Check if a word F is a subsequence of a String S
def subsequence(F, S):
    result = False
    i, j = 0, 0
    word = ""
    while i < len(F) and j < len(S):
        if F[i] == S[j]:
            word = word + F[i]
            j = j + 1
            i = i + 1
        else:
            j = j + 1
    if word == F:
        result = True
    return result


# Check output
D = {"able", "ale", "apple", "bale", "kangaroo"}
S = "abppplee"

lisy = []
print('Word\t\tFound')
for item in D:
    found = subsequence(item, S)
    print(f'{item}\t\t{found}')
    if found:
        lisy.append(item)

print(f"\nLongest word in {', '.join(D)} that is a subsequence of {S} is {max(lisy)}.")