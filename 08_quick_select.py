def partition(lp, rp):
    p = rp
    rp = p - 1
    
    while True:
        while a[lp] < a[p]:
            lp = lp + 1
        
        while a[rp] > a[p]:
            rp = rp - 1
        
        if lp >= rp:
            break
        else:
            a[lp], a[rp] = a[rp], a[lp]
    
    a[lp], a[p] = a[p], a[lp]
    return lp

def quickselect(k_low_index, lp, rp):
    if rp - lp <= 0:
        return a[lp]
    
    p = partition(lp, rp)
    if k_low_index < p:
        quickselect(k_low_index, lp, p - 1)
    elif k_low_index > p:
        quickselect(k_low_index, p + 1, rp)
    else:
        return a[k_low_index]


a = [0, 50, 20, 10, 60, 30]
print('before: '  + str(a))
b = quickselect(1, 0, len(a) - 1)
print('after:  ' + str(a))

print(b)
