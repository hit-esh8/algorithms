class Node():
    def __init__(self, data, next_node=None):
        self.data = data
        self.next_node = next_node

class LinkedList():
    def __init__(self, start_node):
        self.start_node = start_node

    # read at index
    def read(self, index):
        i = 0
        curr = self.start_node
        while i < index:
            curr = curr.next_node
            i = i + 1
        
        if curr:
            return curr.data


    # search data
    def search(self, value):
        i = 0
        curr = self.start_node
        while curr:
            if curr.data == value:
                return i
            else:
                curr = curr.next_node
                i = i + 1
        return None


    # insert
    def insert(self, value, index):
        # 2 hello
        # 0 once   1 upon   2 a   3 time
        #  1        2       3
        # make new node
        new_node = Node(value)
        
        if index == 0:
            new_node.next_node = self.start_node
            self.start_node = new_node
            return
        
        i = 0
        curr = self.start_node
        while i < index - 1:
            i = i + 1
            curr = curr.next_node

        if curr:
            new_node.next_node = curr.next_node
            curr.next_node = new_node

        
    def delete(self, index):
        curr = self.start_node
        if index == 0:
            self.start_node = self.start_node.next_node
        else:
            i = 0
            link_next = None
            while True:
                if i == index - 1:
                    curr.next_node = curr.next_node.next_node
                    return

                i = i + 1
                curr = curr.next_node


    def get_list(self):
        dco = self.start_node
        out = []
        while dco:
            out += [dco.data]
            dco = dco.next_node
        return out


n1 = Node("once")
n2 = Node("upon")
n1.next_node = n2
n3 = Node("a")
n2.next_node = n3
n4 = Node("time")
n3.next_node = n4

ll = LinkedList(n1)

ll.insert('hello', 2)
print(ll.read(1))
print(ll.get_list())
print(ll.search('hello'))
ll.delete(5)
print(ll.get_list())