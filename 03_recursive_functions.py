import sys

# f(5) = 5 * 4 * 3 * 2 * 1
# f(n) = n * (n - 1) * (n - 2) * (n - 3) * 1
def fact(n):
    if n == 1:
        return 1
    else:
        return n * fact(n - 1)


def sumf(n):
    if n == 1:
        return 1
    if n == 0:
        return 0

    return n - 1 + sumf(n - 2)


def mc91(n):
    """McCarth 91 function."""
    if n > 100:
        return n - 10
    else:
        return mc91(mc91(n + 11))


def countdown(n):
    print(n)
    if n == 0:
        return
    countdown(n - 1)


if len(sys.argv) > 1:
    n = int(sys.argv[2])
    t = sys.argv[1]
    if t == 'f':
        print(fact(n))
    elif t == 's':
        print(sumf(n))
    elif t == 'm':
        print(mc91(n))
    elif t == 'c':
        countdown(n)
