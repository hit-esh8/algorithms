# Bubble sort
# In each passthrough, the highest unsorted value "bubbles" up to 
# its correct position, rightmost. Goes on swapping multiple times in a passthrough.
# a = [4, 2, 7, 1, 3]
# out <- [1, 2, 3, 4, 7]

def bubble_sort(a):
    i = 0
    while i < len(a):
        j = 0
        while j < len(a) - 1:
            if a[j+1] < a[j]:
                a[j+1], a[j] = a[j], a[j+1]
            j = j + 1
        i = i + 1
    return a