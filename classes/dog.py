class Dog():
    def __init__(self, name, **hello):
        self.name = name
        self.age = hello
    
    def sit(self):
        print(f'{self.name} is now sitting.')


gel = {'dd': 2, 5: 4}
m = Dog('bruno', gel)
print(m.name)
for i, v in m.age.items():
    print(f'{i} - {v}')
m.sit()